package co.chikemgbemena.cocktailsnappyshopper.data.source.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface DrinksService {

    @GET("/api/json/v1/1/search.php")
    fun searchDrink(@Query("s") query: String): Single<NetworkDrinks>
}