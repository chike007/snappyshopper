package co.chikemgbemena.cocktailsnappyshopper.usecase

import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.core.domain.ReactiveInteractor
import io.reactivex.Single
import javax.inject.Inject

class SaveFavouriteDrinkUseCase @Inject constructor(
    private val repository: DrinksDataSourceContract.Repository
) : ReactiveInteractor.SendInteractor<Drink, Unit> {

    override fun getSingle(params: Drink): Single<Unit> = Single.just(repository.insertFavourite(params))
}