package co.chikemgbemena.cocktailsnappyshopper.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DrinksScope