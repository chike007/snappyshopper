package co.chikemgbemena.cocktailsnappyshopper.usecase

import co.chikemgbemena.cocktailsnappyshopper.DummyData
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class SearchDrinkUseCaseTest {

    private lateinit var searchDrinkUseCase: SearchDrinkUseCase
    private val mockRepository: DrinksDataSourceContract.Repository = mock()

    @Before
    fun `set up`() {
        searchDrinkUseCase = SearchDrinkUseCase(mockRepository)
    }

    @Test
    fun `search drink from repository, return drinks`() {
        val query = "margarita"
        whenever(mockRepository.searchDrink(query))
            .thenReturn(
                Single.just(
                    listOf(DummyData.getDrink())
                )
            )

        val test = searchDrinkUseCase.getBehaviorStream(query).test()

        verify(mockRepository).searchDrink(query)

        test.assertNoErrors()
        test.assertComplete()
        test.assertValueCount(1)
        test.assertValue(listOf(DummyData.getDrink()))
    }

    @Test
    fun `search drink from repository, return empty`() {
        val query = "notfound"
        whenever(mockRepository.searchDrink(query)).thenReturn(
            Single.just(listOf())
        )

        val test = searchDrinkUseCase.getBehaviorStream(query).test()

        verify(mockRepository).searchDrink(query)

        test.assertValueCount(1)
        test.assertValue(listOf())
    }

    @Test
    fun `search drink from repository, get error`() {
        val throwable = Throwable()
        val query = "margarita"
        whenever(mockRepository.searchDrink(query)).thenReturn(Single.error(throwable))

        val test = searchDrinkUseCase.getBehaviorStream(query).test()

        verify(mockRepository).searchDrink(query)

        test.assertNoValues()
        test.assertNotComplete()
        test.assertError(throwable)
        test.assertValueCount(0)
    }
}