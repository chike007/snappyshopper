package co.chikemgbemena.cocktailsnappyshopper.data.mapper

import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.NetworkDrink
import co.chikemgbemena.core.domain.Mapper
import javax.inject.Inject

class NetworkDrinkMapper @Inject constructor() : Mapper<NetworkDrink, Drink>() {

    override fun mapFrom(from: NetworkDrink): Drink {
        return Drink(
            id = from.idDrink,
            name = from.strDrink,
            instruction = from.strInstructions,
            thumb = from.strDrinkThumb,
            category = from.strCategory
        )
    }
}