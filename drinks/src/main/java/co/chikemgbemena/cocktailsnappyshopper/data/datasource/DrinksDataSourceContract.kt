package co.chikemgbemena.cocktailsnappyshopper.data.datasource

import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface DrinksDataSourceContract {

    interface Repository {
        fun searchDrink(query: String): Single<List<Drink>?>
        fun insertFavourite(drink: Drink)
        fun fetchAllFavourites(): Observable<List<Drink>?>
        fun deleteFavouriteById(id: String)
    }

     interface Local {
        fun insert(drink: Drink)
        fun fetchAll(): Observable<List<Drink>?>
        fun deleteById(id: String)
    }

    interface Remote {
        fun searchDrink(query: String): Single<List<Drink>?>
    }
}