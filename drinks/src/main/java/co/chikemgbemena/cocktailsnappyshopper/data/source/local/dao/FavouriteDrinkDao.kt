package co.chikemgbemena.cocktailsnappyshopper.data.source.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.model.DbFavouriteDrink
import io.reactivex.Observable

@Dao
interface FavouriteDrinkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(favouriteDrink: DbFavouriteDrink): Long

    @Query("SELECT * FROM favourite_drink")
    fun fetchAll(): Observable<List<DbFavouriteDrink>?>

    @Query("DELETE FROM favourite_drink WHERE id = :id")
    fun deleteById(id: String)
}