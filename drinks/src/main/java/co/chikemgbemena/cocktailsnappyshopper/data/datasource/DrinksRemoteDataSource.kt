package co.chikemgbemena.cocktailsnappyshopper.data.datasource

import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.NetworkDrink
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.DrinksService
import co.chikemgbemena.core.domain.Mapper
import io.reactivex.Single

class DrinksRemoteDataSource(
    private val service: DrinksService,
    private val dataMapper: Mapper<NetworkDrink, Drink>
) : DrinksDataSourceContract.Remote {

    override fun searchDrink(query: String): Single<List<Drink>?> {
        return service.searchDrink(query).map { result ->
            result.drinks?.map {
                dataMapper.mapFrom(it)
            }
        }
    }
}