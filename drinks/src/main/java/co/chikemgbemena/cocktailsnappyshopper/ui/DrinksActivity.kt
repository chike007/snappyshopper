package co.chikemgbemena.cocktailsnappyshopper.ui

import android.os.Bundle
import co.chikemgbemena.cocktailsnappyshopper.R
import co.chikemgbemena.cocktailsnappyshopper.databinding.ActivityDrinksBinding
import co.chikemgbemena.cocktailsnappyshopper.di.DrinksDH
import co.chikemgbemena.cocktailsnappyshopper.ui.search.SearchDrinkFragment
import co.chikemgbemena.core.application.BaseActivity
import co.chikemgbemena.core.extensions.replaceFragment
import co.chikemgbemena.core.extensions.viewBinding

class DrinksActivity : BaseActivity() {

    private val binding by viewBinding(ActivityDrinksBinding::inflate)
    val component by lazy { DrinksDH.drinksComponent() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        component.inject(this)
        if (savedInstanceState == null) {
            replaceFragment(
                SearchDrinkFragment(),
                "SearchDrinkFragment",
                R.id.container,
                true
            )
        }
    }
}
