package co.chikemgbemena.cocktailsnappyshopper.di

import co.chikemgbemena.core.application.Application

object DrinksDH {

    private var drinksComponent: DrinksComponent? = null

    fun drinksComponent(): DrinksComponent {
        if (drinksComponent == null)
            drinksComponent =
                DaggerDrinksComponent.builder().coreComponent(Application.coreComponent).build()
        return drinksComponent as DrinksComponent
    }

    fun destroyDrinksComponent() {
        drinksComponent = null
    }
}