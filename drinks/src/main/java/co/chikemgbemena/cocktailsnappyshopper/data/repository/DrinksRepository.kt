package co.chikemgbemena.cocktailsnappyshopper.data.repository

import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import io.reactivex.Observable
import io.reactivex.Single

class DrinksRepository(
    private val remote: DrinksDataSourceContract.Remote,
    private val local: DrinksDataSourceContract.Local
) : DrinksDataSourceContract.Repository {

    override fun searchDrink(query: String): Single<List<Drink>?> = remote.searchDrink(query)

    override fun insertFavourite(drink: Drink) = local.insert(drink)

    override fun fetchAllFavourites(): Observable<List<Drink>?> = local.fetchAll()

    override fun deleteFavouriteById(id: String) = local.deleteById(id)
}