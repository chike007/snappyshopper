package co.chikemgbemena.cocktailsnappyshopper.ui.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import co.chikemgbemena.cocktailsnappyshopper.R
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.databinding.FragmentSearchDrinkBinding
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksActivity
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksViewModel
import co.chikemgbemena.cocktailsnappyshopper.ui.adapter.DrinksAdapter
import co.chikemgbemena.cocktailsnappyshopper.ui.detail.DrinkDetailFragment
import co.chikemgbemena.cocktailsnappyshopper.ui.favourites.FavouriteDrinksFragment
import co.chikemgbemena.core.util.RxSearchObservable
import co.chikemgbemena.core.extensions.gone
import co.chikemgbemena.core.extensions.replaceFragment
import co.chikemgbemena.core.extensions.viewBinding
import co.chikemgbemena.core.extensions.visible
import co.chikemgbemena.core.presentation.EventObserver
import co.chikemgbemena.core.presentation.Resource
import co.chikemgbemena.core.presentation.ResourceState
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SearchDrinkFragment : Fragment() {

    private val binding by viewBinding(FragmentSearchDrinkBinding::bind)
    private var compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var viewModelFactory: DrinksViewModel.DrinksViewModelFactory
    private var drinksAdapter: DrinksAdapter? = null

    private val viewModel: DrinksViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(DrinksViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search_drink, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as DrinksActivity).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.searchDrinkLiveData.observe(viewLifecycleOwner, EventObserver { onResult(it) })
        initRxSearch()
        binding.btnFavourites.setOnClickListener {
            (requireActivity() as DrinksActivity).replaceFragment(
                FavouriteDrinksFragment(),
                "FavouriteDrinksFragment",
                R.id.container,
                true
            )
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    private fun initRxSearch() {
        compositeDisposable.add(
            RxSearchObservable.fromView(binding.searchView)
                .debounce(1500, TimeUnit.MILLISECONDS)
                .filter { text ->
                    if (text.isEmpty()) {
                        binding.rvDrinks.post {
                            binding.rvDrinks.gone()
                        }
                        false
                    } else {
                        true
                    }
                }
                .distinctUntilChanged()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { result ->
                    viewModel.searchDrink(result)
                })
    }

    private fun onResult(resource: Resource<List<Drink>?>) {
        val progressBar = binding.progressBar
        resource.let { it ->
            when (it.state) {
                ResourceState.LOADING -> {
                    binding.tvNotFound.gone()
                    progressBar.visible()
                    binding.rvDrinks.gone()
                }
                ResourceState.SUCCESS -> {
                    progressBar.gone()
                    initRecyclerView(it.data)
                }
                ResourceState.ERROR -> {
                    progressBar.gone()
                    it.message?.let {
                        Toast.makeText(requireActivity(), it, Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
    }

    private fun initRecyclerView(drinks: List<Drink>?) {
        if (!drinks.isNullOrEmpty()) {
            drinksAdapter = DrinksAdapter(drinks.sortedBy {
                it.name
            }) {
                viewModel.selectedDrink = it
                (requireActivity() as DrinksActivity).replaceFragment(
                    DrinkDetailFragment(),
                    "DrinkDetailFragment",
                    R.id.container,
                    true
                )
            }

            with(binding.rvDrinks) {
                visible()
                layoutManager = LinearLayoutManager(requireActivity())
                adapter = drinksAdapter
            }
        } else {
            with(binding) {
                progressBar.gone()
                rvDrinks.gone()
                tvNotFound.visible()
            }
        }
    }
}
