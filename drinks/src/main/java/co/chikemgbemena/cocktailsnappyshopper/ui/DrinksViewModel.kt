package co.chikemgbemena.cocktailsnappyshopper.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.di.DrinksDH
import co.chikemgbemena.core.di.networking.Scheduler
import co.chikemgbemena.core.domain.ReactiveInteractor
import co.chikemgbemena.core.extensions.setError
import co.chikemgbemena.core.extensions.setLoading
import co.chikemgbemena.core.extensions.setSuccess
import co.chikemgbemena.core.presentation.Event
import co.chikemgbemena.core.presentation.Resource
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class DrinksViewModel @Inject constructor(
    private val searchDrinkUseCase: ReactiveInteractor.RetrieveInteractor<String, List<Drink>?>,
    private val saveFavouriteDrinkUseCase: ReactiveInteractor.SendInteractor<Drink, Unit>,
    private val getFavouriteDrinksUseCase: ReactiveInteractor.RetrieveInteractor<Unit, List<Drink>?>,
    private val deleteFavouriteDrinkUseCase: ReactiveInteractor.DeleteInteractor<String, Unit>,
    private val appScheduler: Scheduler
) : ViewModel() {

    var selectedDrink: Drink? = null
    private val _searchDrinkLiveData = MutableLiveData<Event<Resource<List<Drink>?>>>()
    val searchDrinkLiveData: LiveData<Event<Resource<List<Drink>?>>>
        get() = _searchDrinkLiveData

    private val _saveFavouriteDrinkLiveData = MutableLiveData<Event<Resource<Unit>>>()
    val saveFavouriteDrinkLiveData: LiveData<Event<Resource<Unit>>>
        get() = _saveFavouriteDrinkLiveData

    private val _favouriteDrinksLiveData = MutableLiveData<Event<Resource<List<Drink>>>>()
    val favouriteDrinksLiveData: LiveData<Event<Resource<List<Drink>>>>
        get() = _favouriteDrinksLiveData

    private val _deleteFavouriteDrinkLiveData = MutableLiveData<Event<Resource<Unit>>>()
    val deleteFavouriteDrinkLiveData: LiveData<Event<Resource<Unit>>>
        get() = _deleteFavouriteDrinkLiveData

    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
        DrinksDH.destroyDrinksComponent()
    }

    fun searchDrink(query: String) {

        compositeDisposable.add(searchDrinkUseCase.getBehaviorStream(query)
            .doOnSubscribe {
                _searchDrinkLiveData.setLoading()
            }
            .subscribeOn(appScheduler.io())
            .observeOn(appScheduler.mainThread())
            .subscribe(
                { _searchDrinkLiveData.setSuccess(it)
                },
                { _searchDrinkLiveData.setError(it.message)
                }
            )
        )
    }

    fun saveFavourite() {
        selectedDrink?.let { it ->
            compositeDisposable.add(saveFavouriteDrinkUseCase.getSingle(it)
                .doOnSubscribe {
                    _saveFavouriteDrinkLiveData.setLoading()
                }
                .subscribeOn(appScheduler.io())
                .observeOn(appScheduler.mainThread())
                .subscribe(
                    { _saveFavouriteDrinkLiveData.setSuccess(it)
                    },
                    { _saveFavouriteDrinkLiveData.setError(it.message)
                    }
                )
            )
        }
    }

    fun getFavouriteDrinks() {
        compositeDisposable.add(getFavouriteDrinksUseCase.getBehaviorStream()
            .doOnSubscribe {
                _favouriteDrinksLiveData.setLoading()
            }
            .subscribeOn(appScheduler.io())
            .observeOn(appScheduler.mainThread())
            .subscribe(
                { _favouriteDrinksLiveData.setSuccess(it)
                },
                { _favouriteDrinksLiveData.setError(it.message)
                }
            )
        )
    }

    fun deleteFavouriteDrink() {
        selectedDrink?.let {
            compositeDisposable.add(deleteFavouriteDrinkUseCase.getSingle(it.id)
                .doOnSubscribe {
                    _deleteFavouriteDrinkLiveData.setLoading()
                }
                .subscribeOn(appScheduler.io())
                .observeOn(appScheduler.mainThread())
                .subscribe(
                    { _deleteFavouriteDrinkLiveData.setSuccess(it)
                    },
                    { _deleteFavouriteDrinkLiveData.setError(it.message)
                    }
                )
            )
        }
    }

    @Suppress("UNCHECKED_CAST")
    class DrinksViewModelFactory(
        private val scheduler: Scheduler,
        private val searchDrinkUseCase: ReactiveInteractor.RetrieveInteractor<String, List<Drink>?>,
        private val saveFavouriteDrinkUseCase: ReactiveInteractor.SendInteractor<Drink, Unit>,
        private val getFavouriteDrinksUseCase: ReactiveInteractor.RetrieveInteractor<Unit, List<Drink>?>,
        private val deleteFavouriteDrinkUseCase: ReactiveInteractor.DeleteInteractor<String, Unit>
        ) :
        ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return DrinksViewModel(
                searchDrinkUseCase,
                saveFavouriteDrinkUseCase,
                getFavouriteDrinksUseCase,
                deleteFavouriteDrinkUseCase,
                scheduler
            ) as T
        }
    }
}