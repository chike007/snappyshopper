package co.chikemgbemena.cocktailsnappyshopper

import androidx.annotation.VisibleForTesting
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.NetworkDrink
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.NetworkDrinks

@VisibleForTesting(otherwise = VisibleForTesting.NONE)
object DummyData {

    fun getNetworkDrinks(): NetworkDrinks {

        val drinks = arrayListOf<NetworkDrink>()

        drinks.add(
            NetworkDrink(
                "11007",
                "Margarita",
                null,
                "IBA,ContemporaryClassic",
                "Ordinary Drink",
                "Contemporary Classics",
                "Alcoholic",
                "Cocktail glass",
                "\"Rub the rim of the glass with the lime slice to make the salt stick " +
                        "to it. Take care to moisten only the outer rim and sprinkle the salt on it. " +
                        "The salt should present to the lips of the imbiber and never mix into " +
                        "the cocktail. Shake the other ingredients with ice, then carefully " +
                        "pour into the glass.\"",
                "Tequila",
                "Triple sec",
                "Lime juice",
                "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg"
            )
        )
        drinks.add(
            NetworkDrink(
                "11118",
                "Blue Margarita",
                null,
                null,
                "Ordinary Drink",
                null,
                "Alcoholic",
                "Cocktail glass",
                "Rub rim of cocktail glass with lime juice. Dip rim in coarse salt. " +
                        "Shake tequila, blue curacao, and lime juice with ice, strain into " +
                        "the salt-rimmed glass, and serve.",
                "Tequila",
                "Blue Curacao",
                "Lime juice",
                "https://www.thecocktaildb.com/images/media/drink/bry4qh1582751040.jpg"
            )
        )
        return NetworkDrinks(drinks)
    }

    fun getDrink(): Drink  = Drink(
        "11118",
        "Blue Margarita",
        "Rub rim of cocktail glass with lime juice. Dip rim in coarse salt. " +
                "Shake tequila, blue curacao, and lime juice with ice, strain into " +
                "the salt-rimmed glass, and serve.",
        "Ordinary Drink",
        "https://www.thecocktaildb.com/images/media/drink/bry4qh1582751040.jpg",
        false
    )
}