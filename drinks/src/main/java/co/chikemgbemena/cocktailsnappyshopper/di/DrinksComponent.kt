package co.chikemgbemena.cocktailsnappyshopper.di

import android.content.Context
import androidx.room.Room
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksActivity
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksLocalDataSource
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksRemoteDataSource
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.mapper.FetchFavouritesDataMapper
import co.chikemgbemena.cocktailsnappyshopper.data.mapper.NetworkDrinkMapper
import co.chikemgbemena.cocktailsnappyshopper.data.mapper.SaveFavouriteDataMapper
import co.chikemgbemena.cocktailsnappyshopper.data.repository.DrinksRepository
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.DrinksDatabase
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.DrinksService
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksViewModel
import co.chikemgbemena.cocktailsnappyshopper.ui.detail.DrinkDetailFragment
import co.chikemgbemena.cocktailsnappyshopper.ui.favourites.FavouriteDrinksFragment
import co.chikemgbemena.cocktailsnappyshopper.ui.search.SearchDrinkFragment
import co.chikemgbemena.cocktailsnappyshopper.usecase.DeleteFavouriteDrinkUseCase
import co.chikemgbemena.cocktailsnappyshopper.usecase.GetFavouriteDrinksUseCase
import co.chikemgbemena.cocktailsnappyshopper.usecase.SaveFavouriteDrinkUseCase
import co.chikemgbemena.cocktailsnappyshopper.usecase.SearchDrinkUseCase
import co.chikemgbemena.core.di.CoreComponent
import co.chikemgbemena.core.di.networking.Scheduler
import co.chikemgbemena.core.domain.ReactiveInteractor
import dagger.Component
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@DrinksScope
@Component(dependencies = [CoreComponent::class], modules = [DrinksModule::class])
interface DrinksComponent {
    fun inject(drinksActivity: DrinksActivity)
    fun inject(searchDrinkFragment: SearchDrinkFragment)
    fun inject(drinkDetailFragment: DrinkDetailFragment)
    fun inject(favouriteDrinksFragment: FavouriteDrinksFragment)
}

@Module
class DrinksModule {

    @Provides
    @DrinksScope
    fun provideDrinksRepository(
        remote: DrinksDataSourceContract.Remote,
        local: DrinksDataSourceContract.Local
    ): DrinksDataSourceContract.Repository = DrinksRepository(remote, local)

    @Provides
    @DrinksScope
    fun provideSearchDrinkViewModelFactory(
        scheduler: Scheduler,
        searchDrinkUseCase: ReactiveInteractor.RetrieveInteractor<String, List<Drink>?>,
        saveFavouriteDrinkUseCase: ReactiveInteractor.SendInteractor<Drink, Unit>,
        getFavouriteDrinksUseCase: ReactiveInteractor.RetrieveInteractor<Unit, List<Drink>?>,
        deleteFavouriteDrinkUseCase: ReactiveInteractor.DeleteInteractor<String, Unit>
        ): DrinksViewModel.DrinksViewModelFactory {
        return DrinksViewModel.DrinksViewModelFactory(
            scheduler,
            searchDrinkUseCase,
            saveFavouriteDrinkUseCase,
            getFavouriteDrinksUseCase,
            deleteFavouriteDrinkUseCase
        )
    }

    @Provides
    @DrinksScope
    internal fun provideDrinksDatabase(context: Context): DrinksDatabase {
        return Room.databaseBuilder(
            context,
            DrinksDatabase::class.java,
            DrinksDatabase.DB_NAME
        ).allowMainThreadQueries().build()
    }

    @Provides
    @DrinksScope
    fun provideRemoteData(
        service: DrinksService,
        dataMapper: NetworkDrinkMapper
    ): DrinksDataSourceContract.Remote = DrinksRemoteDataSource(service, dataMapper)

    @Provides
    @DrinksScope
    fun provideLocalData(
        database: DrinksDatabase,
        saveFavouriteDataMapper: SaveFavouriteDataMapper,
        fetchFavouritesDataMapper: FetchFavouritesDataMapper
    ): DrinksDataSourceContract.Local = DrinksLocalDataSource(
        database,
        saveFavouriteDataMapper,
        fetchFavouritesDataMapper
    )

    @Provides
    @DrinksScope
    fun provideDrinksService(retrofit: Retrofit): DrinksService =
        retrofit.create(DrinksService::class.java)

    @Provides
    @DrinksScope
    fun provideGetFavouriteDrinksUseCase(repo: DrinksDataSourceContract.Repository):
            ReactiveInteractor.RetrieveInteractor<Unit?, List<Drink>?> = GetFavouriteDrinksUseCase(repo)

    @Provides
    @DrinksScope
    fun provideSearchDrinkUseCase(repo: DrinksDataSourceContract.Repository):
            ReactiveInteractor.RetrieveInteractor<String, List<Drink>?> = SearchDrinkUseCase(repo)

    @Provides
    @DrinksScope
    fun provideSaveFavouriteDrinksUseCase(repo: DrinksDataSourceContract.Repository):
            ReactiveInteractor.SendInteractor<Drink, Unit> = SaveFavouriteDrinkUseCase(repo)

    @Provides
    @DrinksScope
    fun provideDeleteFavouriteDrinkUseCase(repo: DrinksDataSourceContract.Repository):
            ReactiveInteractor.DeleteInteractor<String, Unit> = DeleteFavouriteDrinkUseCase(repo)
}