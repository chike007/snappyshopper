package co.chikemgbemena.core.extensions

import androidx.lifecycle.MutableLiveData
import co.chikemgbemena.core.presentation.Event
import co.chikemgbemena.core.presentation.Resource
import co.chikemgbemena.core.presentation.ResourceState

fun <T> MutableLiveData<Event<Resource<T>>>.setSuccess(data: T?) =
    postValue(Event(Resource(ResourceState.SUCCESS, data)))

fun <T> MutableLiveData<Event<Resource<T>>>.setLoading() =
    postValue(Event(Resource(ResourceState.LOADING, value?.peekContent()?.data)))

fun <T> MutableLiveData<Event<Resource<T>>>.setError(message: String? = null) =
    postValue(Event(Resource(ResourceState.ERROR, value?.peekContent()?.data, message)))
