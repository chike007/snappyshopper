package co.chikemgbemena.cocktailsnappyshopper.data.source.local.dao

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.DrinksDatabase
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.model.DbFavouriteDrink
import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.*
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class FavouriteDrinkDaoTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private lateinit var dao: FavouriteDrinkDao
    private lateinit var database: DrinksDatabase
    private val dbFavouriteDrink = DbFavouriteDrink(
        "2",
        "Margarita",
        "Rub the rim of the glass with the lime slice to make the salt stick to it. " +
                "Take care to moisten only the outer rim and sprinkle the salt on it. The salt" +
                " should present to the lips of the imbiber and never mix into the cocktail. " +
                "Shake the other ingredients with ice, then carefully pour into the glass.",
        "https://www.thecocktaildb.com/images/media/drink/5noda61589575158.jpg",
    "Alchol"
    )

    @Before
    fun setUp() {
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext<Context>(),
            DrinksDatabase::class.java).build()
        dao = database.favouriteDrinkDao
    }

    @Test
    fun saveFavouriteDrink_returnInsertedId() {
        val verifyInserted = dao.insert(dbFavouriteDrink)
        Assert.assertTrue(verifyInserted >= 0)
    }

    @Test
    fun getFavourites_returnSameResultAsSaved() {
        val dbFavouriteDrink2 = DbFavouriteDrink(
            "3",
            "Blue Margarita",
            "Rub rim of cocktail glass with lime juice. Dip rim in coarse salt. " +
                    "Shake tequila, blue curacao, and lime juice with ice, " +
                    "strain into the salt-rimmed glass, and serve.",
            "https://www.thecocktaildb.com/images/media/drink/bry4qh1582751040.jpg",
            "Alchol"
        )
        dao.insert(dbFavouriteDrink)
        dao.insert(dbFavouriteDrink2)

        val expectedList = mutableListOf<DbFavouriteDrink>()
        expectedList.add(dbFavouriteDrink)
        expectedList.add(dbFavouriteDrink2)
        val result = dao.fetchAll().test()
        result.assertValue(expectedList)
    }


    @After
    fun tearDown() {
        database.close()
    }
}