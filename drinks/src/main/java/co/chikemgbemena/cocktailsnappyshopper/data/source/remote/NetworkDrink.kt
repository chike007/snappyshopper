package co.chikemgbemena.cocktailsnappyshopper.data.source.remote

data class NetworkDrinks(
    val drinks: List<NetworkDrink>?
)

data class NetworkDrink (
    val idDrink: String,
    val strDrink: String,
    val strDrinkAlternate: String?,
    val strTags: String?,
    val strCategory: String,
    val strIBA: String?,
    val strAlcoholic: String?,
    val strGlass: String?,
    val strInstructions: String,
    val strIngredient1: String?,
    val strIngredient2: String?,
    val strIngredient3: String?,
    val strDrinkThumb: String
)
