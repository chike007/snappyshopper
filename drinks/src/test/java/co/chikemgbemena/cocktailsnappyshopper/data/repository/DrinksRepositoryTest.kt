package co.chikemgbemena.cocktailsnappyshopper.data.repository

import co.chikemgbemena.cocktailsnappyshopper.DummyData
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Before
import org.junit.Test

class DrinksRepositoryTest {

    private lateinit var repository: DrinksRepository

    private val mockRemoteSource: DrinksDataSourceContract.Remote = mock()
    private val mockLocalSource: DrinksDataSourceContract.Local = mock()

    @Before
    fun `set up`() {
        repository = DrinksRepository(mockRemoteSource, mockLocalSource)
    }

    @Test
    fun `search drink remote, return drinks`() {
        val drinks = listOf(DummyData.getDrink())
        whenever(mockRemoteSource.searchDrink("margarita")).thenReturn(
            Single.just(drinks)
        )
        val test = repository.searchDrink("margarita").test()
        test.assertValue(drinks)
    }

    @Test
    fun `save favourite drink, verify local called`() {
        val drink = DummyData.getDrink()
        repository.insertFavourite(drink)
        verify(mockLocalSource).insert(drink)
    }

    @Test
    fun `get favourite drinks, return favourites`() {
        val favourites = listOf(DummyData.getDrink())

        whenever(mockLocalSource.fetchAll()).thenReturn(
            Observable.just(favourites)
        )
        val test = repository.fetchAllFavourites().test()
        test.assertValue(favourites)
    }
}