package co.chikemgbemena.cocktailsnappyshopper.data.datasource

import co.chikemgbemena.cocktailsnappyshopper.DummyData
import co.chikemgbemena.cocktailsnappyshopper.data.mapper.NetworkDrinkMapper
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.DrinksService
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.NetworkDrinks
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Assert
import org.junit.Test

/**
 * Tests for [DrinksRemoteDataSource]
 */
class DrinksRemoteDataSourceTest {

    private val drinksService = mock<DrinksService>()
    private val dataMapper = NetworkDrinkMapper()
    private val throwable = Throwable()

    private var drinksRemoteDataSource: DrinksRemoteDataSource = DrinksRemoteDataSource(
        drinksService, dataMapper
    )

    companion object {
        const val query = "margarita"
    }

    @Test
    fun `search drink, get drinks`() {
        whenever(drinksService.searchDrink(query)).thenReturn(
            Single.just(DummyData.getNetworkDrinks())
        )

        drinksRemoteDataSource.searchDrink(query)
            .test().run {
                assertNoErrors()
                assertValueCount(1)
                Assert.assertEquals(values()[0]!![0].name, "Margarita")
                Assert.assertEquals(values()[0]!![0].id, "11007")
                Assert.assertEquals(values()[0]!![0].isFavourite, false)
            }
    }

    @Test
    fun `search drink, get empty`() {
        whenever(drinksService.searchDrink("notty")).thenReturn(
            Single.just(NetworkDrinks(null))
        )

        drinksRemoteDataSource.searchDrink("notty")
            .test().run {
                Assert.assertEquals(values()?.size, 0)
            }
    }

    @Test
    fun `search drink, return failure`() {
        whenever(drinksService.searchDrink(query)).thenReturn(
            Single.error(throwable)
        )

        drinksRemoteDataSource.searchDrink(query)
            .test()
            .run {
                assertError(throwable)
            }
    }
}