package co.chikemgbemena.cocktailsnappyshopper.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.dao.FavouriteDrinkDao
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.model.DbFavouriteDrink

@Database(entities = [DbFavouriteDrink::class], version = 1, exportSchema = false)
abstract class DrinksDatabase : RoomDatabase() {

    abstract val favouriteDrinkDao: FavouriteDrinkDao

    companion object {
        const val DB_NAME = "drinks.db"
    }
}
