package co.chikemgbemena.cocktailsnappyshopper.usecase

import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import co.chikemgbemena.core.domain.ReactiveInteractor
import io.reactivex.Single
import javax.inject.Inject

class DeleteFavouriteDrinkUseCase @Inject constructor(
    private val repository: DrinksDataSourceContract.Repository
) : ReactiveInteractor.DeleteInteractor<String, Unit> {

    override fun getSingle(params: String): Single<Unit> = Single.just(repository.deleteFavouriteById(params))
}