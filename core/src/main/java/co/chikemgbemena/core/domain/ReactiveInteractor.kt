package co.chikemgbemena.core.domain

import androidx.annotation.NonNull
import androidx.annotation.Nullable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Interfaces for Interactors. This interfaces represent use cases (this means any use case in the application should implement this contract).
 * https://github.com/n26/N26AndroidSamples/blob/master/base/src/main/java/de/n26/n26androidsamples/base/domain/ReactiveInteractor.java
 */
interface ReactiveInteractor {

    /**
     * Retrieves changes from the data layer.
     * It returns an [Flowable] that emits updates for the retrieved object. The returned [Flowable] will never complete, but it can
     * error if there are any problems performing the required actions to serve the data.
     *
     * @param <Object> the type of the retrieved object.
     * @param <Params> required parameters for the retrieve operation.
    </Params></Object> */
    interface RetrieveInteractor<Params, Object> : ReactiveInteractor {

        @NonNull
        fun getBehaviorStream(@Nullable params: Params? = null): Observable<Object>
    }

    /**
     * Sends changes to data layer.
     * It returns a [Single] that will emit the result of the send operation.
     * @param <Result> the type of the send operation result.
     * @param <Params> required parameters for the send.
    </Params></Result> */
    interface SendInteractor<Params, Result> : ReactiveInteractor {
        fun getSingle(params: Params): Single<Result>
    }

    /**
     * The delete interactor is used to delete entities from data layer. The response for the delete operation comes as onNext
     * event in the returned observable.
     * @param <Result> the type of the delete response.
     * @param <Params>   required parameters for the delete.
    </Params></Result> */
    interface DeleteInteractor<Params, Result> : ReactiveInteractor {
        fun getSingle(params: Params): Single<Result>
    }

}

