package co.chikemgbemena.cocktailsnappyshopper.data.mapper

import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.model.DbFavouriteDrink
import co.chikemgbemena.cocktailsnappyshopper.data.source.remote.NetworkDrink
import co.chikemgbemena.core.domain.Mapper
import javax.inject.Inject

class FetchFavouritesDataMapper @Inject constructor() : Mapper<List<DbFavouriteDrink>, List<Drink>>() {

    override fun mapFrom(from: List<DbFavouriteDrink>): List<Drink> {
        return from.map {
            Drink(
                id = it.id,
                name = it.name,
                instruction = it.instruction,
                thumb = it.imageUrl,
                isFavourite = true,
                category = it.category
            )
        }
    }
}