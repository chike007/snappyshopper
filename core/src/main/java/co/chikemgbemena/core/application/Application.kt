package co.chikemgbemena.core.application

import android.app.Application
import co.chikemgbemena.core.di.CoreComponent
import co.chikemgbemena.core.di.DaggerCoreComponent
import co.chikemgbemena.core.di.modules.ApplicationModule
import co.chikemgbemena.core.di.modules.NetworkModule

class Application : Application() {

    companion object {
        lateinit var coreComponent: CoreComponent
    }

    override fun onCreate() {
        super.onCreate()
        initDI()
    }

    private fun initDI() {
        coreComponent = DaggerCoreComponent.builder()
            .applicationModule(ApplicationModule(this))
            .networkModule(NetworkModule).build()
    }
}