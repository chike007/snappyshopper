package co.chikemgbemena.cocktailsnappyshopper.ui.detail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import co.chikemgbemena.cocktailsnappyshopper.R
import co.chikemgbemena.cocktailsnappyshopper.databinding.FragmentDetailDrinkBinding
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksActivity
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksViewModel
import co.chikemgbemena.core.extensions.viewBinding
import co.chikemgbemena.core.presentation.EventObserver
import co.chikemgbemena.core.presentation.Resource
import co.chikemgbemena.core.presentation.ResourceState
import com.bumptech.glide.Glide
import javax.inject.Inject

class DrinkDetailFragment : Fragment()  {

    private val binding by viewBinding(FragmentDetailDrinkBinding::bind)

    @Inject
    lateinit var viewModelFactory: DrinksViewModel.DrinksViewModelFactory

    private val viewModel: DrinksViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(DrinksViewModel::class.java)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as DrinksActivity).component.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_drink, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.selectedDrink?.let { drink ->
            with (binding) {
                Glide.with(this@DrinkDetailFragment).load(drink.thumb)
                    .into(ivDrink)
                tvName.text = drink.name
                tvInstructions.text = drink.instruction

                ivFavourite.setOnClickListener {
                    if (drink.isFavourite) {
                        viewModel.deleteFavouriteDrink()
                    } else {
                        viewModel.saveFavourite()
                    }
                }
            }

            viewModel.saveFavouriteDrinkLiveData.observe(
                viewLifecycleOwner,
                EventObserver { onSaveFavouriteDrinkResult(it) }
            )

            viewModel.deleteFavouriteDrinkLiveData.observe(
                viewLifecycleOwner,
                EventObserver { onDeleteFavouriteDrinkResult(it) }
            )
        }
    }

    private fun onDeleteFavouriteDrinkResult(resource: Resource<Unit>) {
        resource.let { it ->
            when (it.state) {
                ResourceState.LOADING -> {}
                ResourceState.SUCCESS -> {
                    Toast.makeText(
                        requireActivity(),
                        "Favourite removed",
                        Toast.LENGTH_LONG
                    ).show()
                }
                ResourceState.ERROR -> {
                    it.message?.let {
                        Toast.makeText(requireActivity(), it, Toast.LENGTH_LONG).show()
                    }
                }
                else -> {}
            }
        }
    }

    private fun onSaveFavouriteDrinkResult(resource: Resource<Unit>?) {
        resource.let { it ->
            when (it!!.state) {
                ResourceState.LOADING -> { }
                ResourceState.SUCCESS -> {
                    Toast.makeText(
                        requireActivity(),
                        "Added to favourites",
                        Toast.LENGTH_LONG
                    ).show()
                }
                ResourceState.ERROR -> {
                    it.message?.let {
                        Toast.makeText(requireActivity(), it, Toast.LENGTH_LONG).show()
                    }
                }
                else -> {}
            }
        }
    }
}
