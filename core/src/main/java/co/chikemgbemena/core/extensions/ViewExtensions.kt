package co.chikemgbemena.core.extensions

import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ComponentActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.viewbinding.ViewBinding
import java.lang.IllegalStateException
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

inline fun <T : ViewBinding> ComponentActivity.viewBinding(
    crossinline bindingInflater: (LayoutInflater) -> T
) =
    lazy(LazyThreadSafetyMode.NONE) {
        bindingInflater.invoke(layoutInflater)
    }

fun <T : ViewBinding> Fragment.viewBinding(binder: (View) -> T) =
    FragmentViewNullOnDestroy(this, binder)

class FragmentViewNullOnDestroy<T : ViewBinding>(
    val fragment: Fragment,
    val binder: (View) -> T) : ReadOnlyProperty<Fragment, T>,
    DefaultLifecycleObserver {

    private var value: T? = null
    private val viewLifecycleOwnerObserver: Observer<LifecycleOwner?>

    init {
        fragment.lifecycle.addObserver(this)
        viewLifecycleOwnerObserver = Observer {
            val viewLifecycleOwner = it ?: return@Observer

            viewLifecycleOwner.lifecycle.addObserver(object : DefaultLifecycleObserver {
                override fun onDestroy(owner: LifecycleOwner) {
                    value = null
                }
            })
        }
    }

    override fun onCreate(owner: LifecycleOwner) {
        fragment.viewLifecycleOwnerLiveData.observeForever(viewLifecycleOwnerObserver)
    }
    override fun onDestroy(owner: LifecycleOwner) {
        fragment.viewLifecycleOwnerLiveData.removeObserver(viewLifecycleOwnerObserver)
    }

    override fun getValue(thisRef: Fragment, property: KProperty<*>): T {
        return value ?: kotlin.run {
            val lifeCycle = fragment.viewLifecycleOwner.lifecycle

            if (!lifeCycle.currentState.isAtLeast(Lifecycle.State.INITIALIZED)) {
                throw IllegalStateException("Attempted to get binding when fragment view was destroyed")
            }
            binder(thisRef.view!!).also { setValue(thisRef, it) }
        }
    }

    private fun setValue(fragment: Fragment, value: T) {
        fragment.lifecycle.removeObserver(this)
        this.value = value
        fragment.lifecycle.addObserver(this)
    }
}

fun AppCompatActivity.replaceFragment(f: Fragment, tag: String, layout: Int, addToStack: Boolean? = false,
                                      backStackName: String? = null) {
    val ft: FragmentTransaction = this.supportFragmentManager.beginTransaction()
    ft.replace(layout, f, tag)
    if(addToStack == true)
        ft.addToBackStack(backStackName)
    ft.commit()
}