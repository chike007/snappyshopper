package co.chikemgbemena.core.extensions

import co.chikemgbemena.core.di.networking.Scheduler
import io.reactivex.Completable

fun Completable.performOnBack(scheduler: Scheduler): Completable {
    return this.subscribeOn(scheduler.io())
}