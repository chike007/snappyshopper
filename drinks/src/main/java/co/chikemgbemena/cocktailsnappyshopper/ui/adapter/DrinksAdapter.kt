package co.chikemgbemena.cocktailsnappyshopper.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.databinding.ItemDrinkBinding
import com.bumptech.glide.Glide

class DrinksAdapter(
    drinks: List<Drink>,
    private val onDrinkClicked: ((Drink) -> Unit)):
    ListAdapter<Drink, DrinksAdapter.DrinkItemViewHolder>(DiffCallback()) {

    init {
        submitList(drinks)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkItemViewHolder {
        val binding: ItemDrinkBinding = ItemDrinkBinding.inflate(
            LayoutInflater.from(parent.context), parent, false)
        return DrinkItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DrinkItemViewHolder, position: Int) {
        getItem(position)?.let(holder::bindView)
    }

    inner class DrinkItemViewHolder(private val binding: ItemDrinkBinding):
            RecyclerView.ViewHolder(binding.root) {

        fun bindView(drink: Drink) {
            binding.run {
                tvDrinkName.text = drink.name
                tvCategory.text = drink.category
                Glide.with(itemView.context).load(drink.thumb)
                    .into(ivDrink)
                itemView.setOnClickListener {
                    onDrinkClicked.invoke(drink)
                }
            }
        }
    }
}

class DiffCallback: DiffUtil.ItemCallback<Drink>() {
    override fun areItemsTheSame(oldItem: Drink,
                                 newItem: Drink
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Drink,
                                    newItem: Drink
    ): Boolean {
        return oldItem == newItem
    }

}