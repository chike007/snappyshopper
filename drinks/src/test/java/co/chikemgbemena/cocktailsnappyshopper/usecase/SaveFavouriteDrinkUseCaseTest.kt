package co.chikemgbemena.cocktailsnappyshopper.usecase

import co.chikemgbemena.cocktailsnappyshopper.DummyData
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test

class SaveFavouriteDrinkUseCaseTest {

    private lateinit var saveFavouriteDrinkUseCase: SaveFavouriteDrinkUseCase
    private val mockRepository: DrinksDataSourceContract.Repository = mock()

    @Before
    fun `set up`() {
        saveFavouriteDrinkUseCase = SaveFavouriteDrinkUseCase(mockRepository)
    }

    @Test
    fun `save favourite drink in repository`() {
        val drink = DummyData.getDrink()
        val test = saveFavouriteDrinkUseCase.getSingle(drink).test()
        verify(mockRepository).insertFavourite(drink)

        test.assertNoErrors()
        test.assertComplete()
        test.assertValueCount(1)
    }
}
