package co.chikemgbemena.cocktailsnappyshopper.ui.favourites

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import co.chikemgbemena.cocktailsnappyshopper.R
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.databinding.FragmentFavouriteDrinksBinding
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksActivity
import co.chikemgbemena.cocktailsnappyshopper.ui.DrinksViewModel
import co.chikemgbemena.cocktailsnappyshopper.ui.detail.DrinkDetailFragment
import co.chikemgbemena.cocktailsnappyshopper.ui.adapter.DrinksAdapter
import co.chikemgbemena.core.extensions.gone
import co.chikemgbemena.core.extensions.replaceFragment
import co.chikemgbemena.core.extensions.viewBinding
import co.chikemgbemena.core.extensions.visible
import co.chikemgbemena.core.presentation.EventObserver
import co.chikemgbemena.core.presentation.Resource
import co.chikemgbemena.core.presentation.ResourceState
import javax.inject.Inject

class FavouriteDrinksFragment : Fragment()  {

    private val binding by viewBinding(FragmentFavouriteDrinksBinding::bind)

    @Inject
    lateinit var viewModelFactory: DrinksViewModel.DrinksViewModelFactory
    lateinit var drinksAdapter: DrinksAdapter

    private val viewModel: DrinksViewModel by lazy {
        ViewModelProviders.of(requireActivity(), viewModelFactory).get(DrinksViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favourite_drinks, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity as DrinksActivity).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.favouriteDrinksLiveData.observe(viewLifecycleOwner, EventObserver { onResult(it) })
        viewModel.getFavouriteDrinks()
    }

    private fun onResult(resource: Resource<List<Drink>>?) {
        resource?.let { it ->
            when (it.state) {
                ResourceState.LOADING -> {}
                ResourceState.SUCCESS -> {
                    initRecyclerView(it.data)
                }
                ResourceState.ERROR -> {
                    it.message?.let { Toast.makeText(requireActivity(), it, Toast.LENGTH_LONG).show() }
                }
            }
        }
    }

    private fun initRecyclerView(drinks: List<Drink>?) {
        if (!drinks.isNullOrEmpty()) {
            drinksAdapter = DrinksAdapter(drinks.sortedBy {
                it.name }) {
                viewModel.selectedDrink = it
                (requireActivity() as DrinksActivity).replaceFragment(
                    DrinkDetailFragment(),
                    "DrinkDetailFragment",
                    R.id.container,
                    true
                )
            }

            with (binding.rvDrinks) {
                visible()
                layoutManager = LinearLayoutManager(requireActivity())
                adapter = drinksAdapter
            }
        } else {
            with(binding) {
                rvDrinks.gone()
                tvNotFound.visible()
            }
        }
    }
}
