package co.chikemgbemena.cocktailsnappyshopper.usecase

import co.chikemgbemena.cocktailsnappyshopper.DummyData
import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test

class GetFavouriteDrinksUseCaseTest {

    private lateinit var getFavouriteDrinkUseCase: GetFavouriteDrinksUseCase
    private val mockRepository: DrinksDataSourceContract.Repository = mock()

    @Before
    fun `set up`() {
        getFavouriteDrinkUseCase = GetFavouriteDrinksUseCase(mockRepository)
    }

    @Test
    fun `get favourite drinks in repository, return list`() {
        val favouriteDrinks = listOf(DummyData.getDrink())
        whenever(mockRepository.fetchAllFavourites())
            .thenReturn(
                Observable.just(
                    favouriteDrinks
                )
            )

        val test = getFavouriteDrinkUseCase.getBehaviorStream().test()
        verify(mockRepository).fetchAllFavourites()

        test.assertNoErrors()
        test.assertComplete()
        test.assertValueCount(1)
        test.assertValue(favouriteDrinks)
    }
}