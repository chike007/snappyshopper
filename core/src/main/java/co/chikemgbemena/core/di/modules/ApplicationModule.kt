package co.chikemgbemena.core.di.modules

import android.content.Context
import co.chikemgbemena.core.di.networking.AppScheduler
import co.chikemgbemena.core.di.networking.Scheduler
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class ApplicationModule(private val context: Context) {

    @Provides
    @Reusable
    fun provideContext(): Context = context

    @Provides
    @Reusable
    fun provideAppScheduler(): Scheduler = AppScheduler()
}
