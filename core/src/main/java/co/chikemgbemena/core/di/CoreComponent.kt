package co.chikemgbemena.core.di

import android.content.Context
import co.chikemgbemena.core.di.modules.ApplicationModule
import co.chikemgbemena.core.di.modules.NetworkModule
import co.chikemgbemena.core.di.networking.Scheduler
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, NetworkModule::class])
interface CoreComponent {

    fun scheduler(): Scheduler
    fun context(): Context
    fun retrofit(): Retrofit
}