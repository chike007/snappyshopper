package co.chikemgbemena.cocktailsnappyshopper.ui

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import co.chikemgbemena.cocktailsnappyshopper.DummyData
import co.chikemgbemena.cocktailsnappyshopper.data.TestScheduler
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.core.domain.ReactiveInteractor
import co.chikemgbemena.core.presentation.Event
import co.chikemgbemena.core.presentation.Resource
import co.chikemgbemena.core.presentation.ResourceState
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class DrinksViewModelTest {

    private lateinit var viewModel: DrinksViewModel
    private val searchUseCaseMock: ReactiveInteractor.RetrieveInteractor<String,
            List<Drink>?> = mock()
    private val saveFavouriteUseCaseMock: ReactiveInteractor.SendInteractor<Drink, Unit> = mock()
    private val getFavouriteDrinksUseCaseMock:
            ReactiveInteractor.RetrieveInteractor<Unit, List<Drink>?> = mock()
    private val throwable = Throwable()

    @Rule
    @JvmField
    val instantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    companion object {
        val drinks = listOf(DummyData.getDrink())
    }

    @Before
    fun `set up`() {
        viewModel = DrinksViewModel(
            searchUseCaseMock,
            saveFavouriteUseCaseMock,
            getFavouriteDrinksUseCaseMock,
            TestScheduler()
        )
    }

    @Test
    fun `search drink, return success drinks`() {
        val query = "margarita"
        whenever(searchUseCaseMock.getBehaviorStream(query))
            .thenReturn(Observable.just(drinks))

        viewModel.searchDrink(query)

        verify(searchUseCaseMock).getBehaviorStream(query)
        Assert.assertEquals(
            Resource(ResourceState.SUCCESS, drinks, null),
            viewModel.searchDrinkLiveData.value
        )
    }

    @Test
    fun `search drink, return success empty drinks`() {
        val query = "nothingfound"
        val emptyDrinks = listOf<Drink>()
        whenever(searchUseCaseMock.getBehaviorStream(query))
            .thenReturn(Observable.just(emptyDrinks))

        viewModel.searchDrink(query)

        verify(searchUseCaseMock).getBehaviorStream(query)
        Assert.assertEquals(
            Event(Resource(ResourceState.SUCCESS, emptyDrinks, null)).peekContent(),
            viewModel.searchDrinkLiveData.value?.peekContent()
        )
    }

    @Test
    fun `save favourite drink, return success`() {
        whenever(saveFavouriteUseCaseMock.getSingle(DummyData.getDrink()))
            .thenReturn(Single.just(Unit))

        viewModel.selectedDrink = DummyData.getDrink()
        viewModel.saveFavourite()

        verify(saveFavouriteUseCaseMock).getSingle(DummyData.getDrink())
        Assert.assertEquals(
            Event(Resource(ResourceState.SUCCESS, Unit, null)).peekContent(),
            viewModel.saveFavouriteDrinkLiveData.value?.peekContent()
        )
    }

    @Test
    fun `get favourite drinks, return list`() {
        val favouriteDrinks = listOf(DummyData.getDrink())
        whenever(getFavouriteDrinksUseCaseMock.getBehaviorStream())
            .thenReturn(Observable.just(favouriteDrinks))

        viewModel.getFavouriteDrinks()

        verify(getFavouriteDrinksUseCaseMock).getBehaviorStream()
        Assert.assertEquals(
            Event(Resource(ResourceState.SUCCESS, favouriteDrinks, null)).peekContent(),
            viewModel.favouriteDrinksLiveData.value?.peekContent()
        )
    }

    @Test
    fun `search drink, return error`() {
        val query = "error"
        whenever(searchUseCaseMock.getBehaviorStream(query))
            .thenReturn(Observable.error(throwable))

        viewModel.searchDrink(query)

        verify(searchUseCaseMock).getBehaviorStream(query)
        Assert.assertEquals(
            Event(Resource(
                state = ResourceState.ERROR,
                data = null,
                message = throwable.message)
            ).peekContent(),
            viewModel.searchDrinkLiveData.value?.peekContent()
        )
    }
}