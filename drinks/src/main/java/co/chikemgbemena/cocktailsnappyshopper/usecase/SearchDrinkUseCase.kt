package co.chikemgbemena.cocktailsnappyshopper.usecase

import co.chikemgbemena.cocktailsnappyshopper.data.datasource.DrinksDataSourceContract
import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.core.domain.ReactiveInteractor
import io.reactivex.Observable
import javax.inject.Inject

class SearchDrinkUseCase @Inject constructor(
    private val repository: DrinksDataSourceContract.Repository
) : ReactiveInteractor.RetrieveInteractor<String, List<Drink>?> {

    override fun getBehaviorStream(params: String?): Observable<List<Drink>?> =
        repository.searchDrink(params!!).toObservable()
}