package co.chikemgbemena.cocktailsnappyshopper.data.mapper

import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.model.DbFavouriteDrink
import co.chikemgbemena.core.domain.Mapper
import javax.inject.Inject

class SaveFavouriteDataMapper @Inject constructor() : Mapper<Drink, DbFavouriteDrink>() {

    override fun mapFrom(from: Drink): DbFavouriteDrink {

        return DbFavouriteDrink(
            id = from.id,
            name = from.name,
            instruction = from.instruction,
            imageUrl = from.thumb,
            category = from.category

        )
    }
}
