package co.chikemgbemena.cocktailsnappyshopper.data.datasource

import co.chikemgbemena.cocktailsnappyshopper.data.domain.Drink
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.DrinksDatabase
import co.chikemgbemena.cocktailsnappyshopper.data.source.local.model.DbFavouriteDrink
import co.chikemgbemena.core.domain.Mapper
import io.reactivex.Observable

class DrinksLocalDataSource(
    private val db: DrinksDatabase,
    private val saveFavouriteMapper: Mapper<Drink, DbFavouriteDrink>,
    private val fetchFavouritesMapper: Mapper<List<DbFavouriteDrink>, List<Drink>>
) : DrinksDataSourceContract.Local {

    override fun insert(drink: Drink) {
        val dao = db.favouriteDrinkDao
        dao.insert(saveFavouriteMapper.mapFrom(drink))
    }

    override fun fetchAll(): Observable<List<Drink>?> {
        val dao = db.favouriteDrinkDao
        return dao.fetchAll().map {
            fetchFavouritesMapper.mapFrom(it)
        }
    }

    override fun deleteById(id: String) {
        val dao = db.favouriteDrinkDao
        dao.deleteById(id)
    }
}