package co.chikemgbemena.cocktailsnappyshopper.data.domain

data class Drink(
    val id: String,
    val name: String,
    val instruction: String,
    val category: String,
    val thumb: String,
    val isFavourite: Boolean = false
)