package co.chikemgbemena.cocktailsnappyshopper.data.source.local.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favourite_drink")
data class DbFavouriteDrink(
    @PrimaryKey
    val id: String,
    val name: String,
    val instruction: String,
    val imageUrl: String,
    val category: String
)