package co.chikemgbemena.cocktailsnappyshopper.data.source.remote

import co.chikemgbemena.cocktailsnappyshopper.data.DependencyProvider
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException

/**
 * Tests for [SearchServicea]
 */
class DrinksServiceTest {

    private lateinit var service: DrinksService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun init() {
        mockWebServer = MockWebServer()
        service = DependencyProvider
            .getRetrofit(mockWebServer.url("/"))
            .create(DrinksService::class.java)
    }

    @Test
    fun `search drink using query, return drinks 200`() {
        queueResponse {
            setResponseCode(200)
            setBody(DependencyProvider.getResponseFromJson("drinks"))
        }

        service
            .searchDrink("margarita")
            .test()
            .run {
                assertNoErrors()
                assertValueCount(1)
                Assert.assertEquals(values()[0].drinks?.size, 2)
                Assert.assertEquals(values()[0].drinks!![0].strDrink, "Margarita")
                Assert.assertEquals(values()[0].drinks!![0].idDrink, "11007")
            }
    }

    @Test
    fun `search drink using query, return empty 200`() {
        queueResponse {
            setResponseCode(200)
            setBody(DependencyProvider.getResponseFromJson("drinks_empty"))
        }

        service
            .searchDrink("notty")
            .test()
            .run {
                assertNoErrors()
                assertValueCount(1)
                Assert.assertEquals(values()[0].drinks?.size, 0)
            }
    }

    private fun queueResponse(block: MockResponse.() -> Unit) {
        mockWebServer.enqueue(MockResponse().apply(block))
    }

    @After
    @Throws(IOException::class)
    fun `tear down`() {
        mockWebServer.shutdown()
    }
}